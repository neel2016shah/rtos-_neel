//  RTOS Framework - Spring 2022

// No memory protection, no privilege enforcement
// guided by J Losh

// Student Name:
//  NEEL SHAH


//-----------------------------------------------------------------------------
// Hardware Target
//-----------------------------------------------------------------------------

// Target Platform: EK-TM4C123GXL Evaluation Board
// Target uC:       TM4C123GH6PM
// System Clock:    40 MHz

// Hardware configuration:
// 6 Pushbuttons and 5 LEDs, UART
// LEDs on these pins:
// Blue:   PF2 (on-board)
// Red:    PA2
// Orange: PA3
// Yellow: PA4
// Green:  PE0
// PBs on these pins
// PB0:    PC4
// PB1:    PC5
// PB2:    PC6
// PB3:    PC7
// PB4:    PD6
// PB5:    PD7
// UART Interface:
//   U0TX (PA1) and U0RX (PA0) are connected to the 2nd controller
//   The USB on the 2nd controller enumerates to an ICDI interface and a virtual COM port
//   Configured to 115,200 baud, 8N1

//-----------------------------------------------------------------------------
// Device includes, defines, and assembler directives
//-----------------------------------------------------------------------------

#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include "tm4c123gh6pm.h"
#include "uart0.h"
#include "wait.h"

// REQUIRED: correct these bitbanding references for the off-board LEDs
#define BLUE_LED     (*((volatile uint32_t *)(0x42000000 + (0x400253FC-0x40000000)*32 + 2*4))) // on-board blue LED
#define RED_LED      (*((volatile uint32_t *)(0x42000000 + (0x400043FC-0x40000000)*32 + 2*4))) // off-board red LED
#define GREEN_LED    (*((volatile uint32_t *)(0x42000000 + (0x400243FC-0x40000000)*32 + 0*4))) // off-board green LED
#define YELLOW_LED   (*((volatile uint32_t *)(0x42000000 + (0x400043FC-0x40000000)*32 + 4*4))) // off-board yellow LED
#define ORANGE_LED   (*((volatile uint32_t *)(0x42000000 + (0x400043FC-0x40000000)*32 + 3*4))) // off-board orange LED
#define PB0          (*((volatile uint32_t *)(0x42000000 + (0x400063FC-0x40000000)*32 + 4*4)))
#define PB1          (*((volatile uint32_t *)(0x42000000 + (0x400063FC-0x40000000)*32 + 5*4)))
#define PB2          (*((volatile uint32_t *)(0x42000000 + (0x400063FC-0x40000000)*32 + 6*4)))
#define PB3          (*((volatile uint32_t *)(0x42000000 + (0x400063FC-0x40000000)*32 + 7*4)))
#define PB4          (*((volatile uint32_t *)(0x42000000 + (0x400073FC-0x40000000)*32 + 6*4)))
#define PB5          (*((volatile uint32_t *)(0x42000000 + (0x400073FC-0x40000000)*32 + 7*4)))
//
#define BLUE_LED_MASK 4
#define RED_LED_MASK 4
#define ORANGE_LED_MASK 8
#define YELLOW_LED_MASK 16
#define GREEN_LED_MASK 1
#define PB0_MASK 16
#define PB1_MASK 32
#define PB2_MASK 64
#define PB3_MASK 128
#define PB4_MASK 64
#define PB5_MASK 128
#undef DEBUG
char str[300];

extern void swPush();
extern void * getPSP();
extern void swPop();
extern void* dummyPush();
extern uint8_t  getsvcnumber();
extern uint32_t getR0();
//------------------------------------------------------------------------------
// global heap created here just for reference

uint32_t *heap =(uint32_t*)0x20002000;
//------------------------------------------------
// For scheduling priority base
#define MAX_INDEX 8   // max priority  0 to 7 0 means highest
int8_t task_index[MAX_INDEX]; // holds past run task as per relevant priority
bool RoundRobin =false;
bool Preemption=false;
uint32_t CPUtotal =0; //time of CPU run over the all time includes kernel +tasks times
//-----------------------------------------------------------------------------
// RTOS Defines and Kernel Variables
//-----------------------------------------------------------------------------
//
// function pointer
typedef void (*fn)();

// semaphore
#define MAX_SEMAPHORES 5
#define MAX_QUEUE_SIZE 2
typedef struct _semaphore
{   char S_name[16];
    uint16_t count;
    uint16_t queueSize;
    uint32_t processQueue[MAX_QUEUE_SIZE]; // store task index here
} semaphore;

semaphore semaphores[MAX_SEMAPHORES];
#define keyPressed 1
#define keyReleased 2
#define flashReq 3
#define resource 4

// task
#define STATE_INVALID    0 // no task
#define STATE_UNRUN      1 // task has never been run
#define STATE_READY      2 // has run, can resume at any time
#define STATE_DELAYED    3 // has run, but now awaiting timer
#define STATE_BLOCKED    4 // has run, but now blocked by semaphore
#define STATE_HOLD      5 // the task was deleted but still on memory
#define MAX_TASKS 12       // maximum number of valid tasks
uint8_t taskCurrent = 0;   // index of last dispatched task
uint8_t taskCount = 0;     // total number of valid tasks
uint32_t pidCounter = 0;   // incremented on each thread created
//-------------------------------------------------------------------------------------------------
//for shell command
uint32_t Start_timer;
uint32_t Task_timer[MAX_TASKS];  // keeps live data
uint32_t Freezdata[MAX_TASKS];   // frozen data for ps use




//--- for PuTTy input
#define MAX_FIELDS 10
#define MAX_DATA 10
#define MAX_CHARS 100
typedef struct _USER_DATA
{
char buffer[MAX_CHARS+1];
uint8_t fieldCount;
uint8_t fieldPosition[5];
char fieldType[5];
}USER_DATA;

void getsUart0(USER_DATA*data)
{

    int8_t count =0;
 char c;
 c= getcUart0();
    while((count>=0 && count<=80))
     {
        c= getcUart0();
        if (c==8 || c==127)   // backspace
            {
              if (c>0)
               {
                  count=count - 1;
                  putcUart0(c);
               }
              else
              {
                 getcUart0();
                 putcUart0(c);
              }
            }
        else if (c>=32)
            {
                data->buffer[count]=c;
                count =count+1;
                putcUart0(c);
            }
        else if (c==10 || c==13 || count== MAX_CHARS) // for enter
            {
                data->buffer[count]='\0';

                break;
            }



     }

}

void  parseFields(USER_DATA*data)
{
    uint8_t i=0,j=0;
     char c;
     data->fieldCount = 0;

         uint8_t holder=2;
         for( i=0,j=0;data->buffer[i]!='\0';i++)
             {c=data->buffer[i];
                 if(c>=48 && c<=57)
                 {
                     if(holder!=0)
                     {
                         data->fieldType[j]='n';
                         data->fieldPosition[j]=i;
                         j++;
                         data->fieldCount++;

                         holder=0;
                     }
                     else
                     {
                         continue;
                     }

                 }
                 else if(((c>=65 && c<=90)||(c>=97 && c<=122)) )
                 {
                     if (holder!=1)
                     {  data->fieldType[j]='a';
                         data->fieldPosition[j]=i;
                         j++;
                         data->fieldCount++;

                         holder=1;

                     }
                     else
                     {
                         continue;
                     }
                 }
                 else
                 {
                     data->buffer[i]='\0';

                     holder=2;
                 }
             }


}

bool isCommand(USER_DATA* data, const char strCommand[], uint8_t minArguments)
{
    uint8_t p;
    if( data->fieldCount > minArguments )
    {
        for(p=0;strCommand[p]!='\0';p++)
        {
            if(data->buffer[p]==strCommand[p])
            {
                continue;
            }
            else
            {
                return false;

            }
        }
        return true;

    }
    else
    {
        return false;
    }
}
char* getFieldString(USER_DATA* data, uint8_t fieldNumber)
{
    if(data->fieldType[fieldNumber]=='a')
    {
        char* aa=&data->buffer[data->fieldPosition[fieldNumber]];

      return aa; //(char*)data->buffer[data->fieldPosition[fieldNumber]];
    }
        return '\0' ;

}
int32_t getFieldInteger(USER_DATA* data, uint8_t fieldNumber)
    {
          if(data->fieldType[fieldNumber]=='n')
          {
             char* st=&data->buffer[data->fieldPosition[fieldNumber]];
             int i=0;
             int count=0;
             for( i=0; st[i]!='\0';i++)
              {

                 count=count*10 +st[i] -'0';

              }

             return count;
          }


                  return -1 ;


    }
//---------------------------------------------------------------------------------
// REQUIRED: add store and management for the memory used by the thread stacks
//           thread stacks must start on 1 kiB boundaries so mpu can work correctly

struct _tcb
{
    uint8_t state;                 // see STATE_ values above
    uint32_t pid;                  // PID
    fn pFn;                        // function pointer
    void *spInit;                  // original top of stack
    void *sp;                      // current stack pointer
    int8_t priority;               // 0=highest to 7=lowest
    uint32_t ticks;                // ticks until sleep complete
    char name[16];                 // name of task used in ps command
    uint8_t s;                     // index of semaphore that is blocking the thread
} tcb[MAX_TASKS];

//-----------------------------------------------------------------------------
// RTOS Kernel Functions
//-----------------------------------------------------------------------------

// REQUIRED: initialize systick for 1ms system timer
void initRtos()
{
    uint8_t i;
    // no tasks running
    taskCount = 0;
    // clear out tcb records
    for (i = 0; i < MAX_TASKS; i++)
    {
        tcb[i].state = STATE_INVALID;
        tcb[i].pFn = 0;
        if(i<MAX_INDEX)
        {
            task_index[i]=-1;   // making task index initial value -1
        }


    }

}

// REQUIRED: Implement prioritization to 8 levels

int rtosScheduler()
{
    uint8_t current_priority= 8;
     uint8_t next=12;
    static uint8_t task = 0xFF;
  if(RoundRobin)
  {
      bool ok;
          ok = false;
          while (!ok)
          {
              task++;
              if (task >= MAX_TASKS)
                  task = 0;
              ok = (tcb[task].state == STATE_READY || tcb[task].state == STATE_UNRUN);
          }
          return task;

  }
  else
  {
      for(task=0;task<MAX_TASKS;task++)
         {
             if((tcb[task].state == STATE_READY || tcb[task].state == STATE_UNRUN) && current_priority > tcb[task].priority)
             {
                 current_priority =tcb[task].priority;

             }
         }
          // now current priority has highest priority to run
      for(task=0;task<MAX_TASKS;task++)
      {
          if((tcb[task].state == STATE_READY || tcb[task].state == STATE_UNRUN) && current_priority==tcb[task].priority)
          {
              if(task_index[current_priority]==-1)
                 {
                   task_index[current_priority]=task;
                   next=task;
                   return task;
                  }
              if(task_index[current_priority]==task )
              {
                         next=task;
                         continue;
              }
              if(task>task_index[current_priority])
                  {
                      task_index[current_priority]=task;
                       next=task;
                       return task;
                  }
          }
      }
      for(task=0;task<=next;task++)
      {
          if((tcb[task].state == STATE_READY || tcb[task].state == STATE_UNRUN) && current_priority==tcb[task].priority)
          {
              task_index[current_priority]=task;
              return task;
          }
      }
  } return 0;
}

// checking which task is called and allocate stack for it

int giveStackposition(const char task[])
{int p=0;
 int x;
    char a[]= "Idle";
    //char b[]= "Idle2";
    char b[]= "LengthyFn";
    char c[]= "Flash4Hz";
    char d[]= "OneShot";
    char e[]="ReadKeys";
    char f[]="Debounce";
    char g[]="Important";
    char h[]="Uncoop";
    char i[]="Shell";

        for(p;task[p]!='\0';p++)
            {
                if(task[p] == a[p])
                { x =64;
                  continue;
                }
                else if(task[p] == b[p])
                 {    x=128;
                       continue;
                 }
                else if(task[p] == c[p])
                { x= 192;//3*64;
                  continue;
                }
                else if(task[p] == d[p])
                { x=256;// 4*64;
                  continue;
                }
                else if(task[p] == e[p])
                { x= 320;
                  continue;
                }
                else if(task[p] == f[p])
                { x= 384;
                  continue;
                }
                else if(task[p] == g[p])
                { x= 448;
                   continue;
                }
                else if(task[p] == h[p])
                { x= 512;  //8*64
                 continue;
                }
                else if(task[p]==i[p])
                {
                    x=768;//12*64;
                    continue;
                }
                else
                {   x=0;
                  break;
                }
            }
            return x;


}


bool createThread(fn task, const char name[], uint8_t priority, uint32_t stackBytes)
{
    bool ok = false;
    uint8_t i = 0;
    bool found = false;
    // store the thread name
    // allocate stack space and store top of stack in sp and spInit
   // add task if room in task list
    if (taskCount < MAX_TASKS)
    {
        // make sure task not already in list (prevent reentrancy)
        while (!found && (i < MAX_TASKS))
        {
            found = (tcb[i++].pFn == task);
        }
        if (!found)
        {
            // find first available tcb record
            i = 0;
            while (tcb[i].state != STATE_INVALID) {i++;}
            tcb[i].state = STATE_UNRUN;
            tcb[i].pid = pidCounter++;
            tcb[i].pFn = task;
            tcb[i].sp = heap + giveStackposition(name)*sizeof(uint32_t);                       // just writing a different way & for  understanding heap
            tcb[i].spInit = (uint32_t*)0x20002000 + giveStackposition(name)*sizeof(uint32_t);
            tcb[i].priority = priority;
            int n;
            for (n=0;name[n]!='\0';n++)
               {
                  tcb[i].name[n] = name[n];    // add task name in tcb
               }

            // increment task count
            taskCount++;
            ok = true;

// test sp & spinit
#ifdef DEBUG
   sprintf(str, "stack Spinit value: %p\r\n",tcb[i].spInit );
   putsUart0(str);
   sprintf(str, "Sp_value: %p\r\n",tcb[i].sp );
   putsUart0(str);
#endif
        }
    }
    return ok;
}

// REQUIRED: modify this function to restart a thread
void restartThread(fn task)
{
    uint8_t i;
    for(i=0;i<MAX_TASKS;i++)
    {
        if(tcb[i].pFn==task)
        {
           if(tcb[i].state==STATE_HOLD)
           {
               tcb[i].pid=pidCounter++;
               tcb[i].sp=tcb[i].spInit;
               tcb[i].ticks=NULL;    // this things to just make sure if task killed not properly
               //tcb[i].s=0;       //
               tcb[i].state=STATE_UNRUN;
               putsUart0("\rNow Task can run\r\n");
               break;
           }

        }

    }
    if(i==MAX_TASKS)
    {
        putsUart0("\rNo Task for restart\r\n");
    }
}

// REQUIRED: modify this function to destroy a thread
// REQUIRED: remove any pending semaphore waiting
// NOTE: see notes in class for strategies on whether stack is freed or not
void destroyThread(fn task)
{
    uint8_t i,j,k,p;
    for(i=0;i<MAX_TASKS;i++)
    {
        if(tcb[i].pFn==task &&tcb[i].state!=STATE_HOLD)
        {
            if(tcb[i].state==STATE_BLOCKED)
             {



                    for(k=1;k<MAX_SEMAPHORES;k++)         // removing task which is blocked in any semaphore queue
                    {
                        for(j=0;j<MAX_QUEUE_SIZE;j++)
                        {
                            if(semaphores[k].processQueue[j]==i)
                            {
                                if(j==MAX_QUEUE_SIZE -1)
                                {
                                    semaphores[k].processQueue[j]= NULL;
                                    semaphores[k].queueSize--;

                                }
                                else
                                {
                                    semaphores[k].processQueue[j]=semaphores[k].processQueue[j+1] ;
                                    semaphores[k].queueSize--;

                                }


                            }
                        }
                    }
                    tcb[i].state=STATE_HOLD;
             }
             else     // if given task is running /delayed
             {
                for(p=1;p<MAX_SEMAPHORES;p++)
                {
                    if(tcb[i].s==p)   // finding the semaphore which is blocked by  this running task which we want to kill
                    {
                        if(semaphores[p].queueSize>0)
                        {


                            tcb[semaphores[p].processQueue[0]].s=0;  // not needed
                            tcb[semaphores[p].processQueue[0]].state=STATE_READY;
                            semaphores[p].processQueue[0]=semaphores[p].processQueue[1];
                            semaphores[p].queueSize--;
                        }

                    }

                }
                tcb[i].s=0;
                tcb[i].state=STATE_HOLD;
             }

         putsUart0("\r Given task is destroyed successfully\r\n");
          break;
        }
    }
    if(i==MAX_TASKS)
    {
        putsUart0("\r given task not exist or already destroyed\r\n");
    }
}

// REQUIRED: modify this function to set a thread priority
void setThreadPriority(fn task, uint8_t priority)
{ uint8_t i;
 for(i=0;i<MAX_TASKS;i++)
 {
     if(tcb[i].pFn==task)
     {
         tcb[i].priority=priority;
         putsUart0("\rPriority changed successfully\r\n");
         break;
     }


 }
   // yield();   // if priority increase than it makes the task on running faster

}

bool createSemaphore(uint8_t semaphore,const char Name[],uint8_t count)
{
    uint8_t n;
    bool ok = (semaphore < MAX_SEMAPHORES);
    {
        semaphores[semaphore].count = count;

         for (n=0;Name[n]!='\0';n++)
          {
             semaphores[semaphore].S_name[n] = Name[n];    // add task name
          }
    }
    return ok;
}
extern void  setPSP(uint32_t *psp);
extern void  setASP(void);


// REQUIRED: modify this function to start the operating system
// by calling scheduler, setting PSP, ASP bit, and PC
void startRtos()
{
    taskCurrent =rtosScheduler();
   tcb[taskCurrent].state =STATE_READY;
   setPSP(tcb[taskCurrent].sp);
   setASP();
  // taskCurrent=rtosScheduler();

   fn task= tcb[taskCurrent].pFn;
   task();
}

// REQUIRED: modify this function to yield execution back to scheduler using pendsv

void yield()
{
    __asm(" SVC #40");
    __asm("             NOP");

}

// REQUIRED: modify this function to support 1ms system timer
// execution yielded back to scheduler until time elapses using pendsv
void sleep(uint32_t tick)
{
    __asm(" SVC #41");
}

// REQUIRED: modify this function to wait a semaphore using pendsv
void wait(uint8_t s)
{
    __asm(" SVC #42");
}

// REQUIRED: modify this function to signal a semaphore is available using pendsv
void post(uint8_t s)
{
    __asm(" SVC #43");
}

// REQUIRED: modify this function to add support for the system timer
// REQUIRED: in preemptive code, add code to request task switch
void systickIsr()
{
    int i;
    for (i=0;i<=MAX_TASKS ;i++)
    {
        if(tcb[i].state==STATE_DELAYED)
        {
            if(tcb[i].ticks>0)
            {
                tcb[i].ticks--;

                if(tcb[i].ticks==0)
                {
                  tcb[i].state=STATE_READY;
                }
            }

        }
    }

   if(Preemption==true)
   {
       NVIC_INT_CTRL_R=NVIC_INT_CTRL_PEND_SV;     // every milisec its call pendsv
   }



    Start_timer++;
    CPUtotal++;








}





// REQUIRED: in coop and preemptive, modify this function to add support for task switching
// REQUIRED: process UNRUN and READY tasks differently
void pendSvIsr()
{

#ifdef DEBUG
   sprintf(str, "HW PUSH DONE: %p\r\n",getPSP() );
   putsUart0(str);

   sprintf(str, " done TASK WHILE ENTERING IN PENDsVIsr: %s\r\n",tcb[taskCurrent].name);
   putsUart0(str);

#endif
//    BLUE_LED = 1;
    swPush();
    tcb[taskCurrent].sp=getPSP();
#ifdef DEBUG
   sprintf(str, "SW PUSH DONE: %p\r\n",tcb[taskCurrent].sp );
   putsUart0(str);

#endif


   Task_timer[taskCurrent]+=Start_timer;
   taskCurrent =rtosScheduler();   //*****************
   Start_timer=0;


   setPSP(tcb[taskCurrent].sp);   // may required to put inside to if
   if( tcb[taskCurrent].state ==STATE_READY)
   {

       swPop();

#ifdef DEBUG
   sprintf(str, "current sp for pop: %p\r\n",tcb[taskCurrent].sp );
   putsUart0(str);
   sprintf(str, "SW POP DONE: %p\r\n",getPSP());
   putsUart0(str);

#endif


   }
   else
   {
       dummyPush(tcb[taskCurrent].pFn);   // hw push
       tcb[taskCurrent].state =STATE_READY;



   }


   freezdata(); // this function make copy of live data



}

// REQUIRED: modify this function to add support for the service call
// REQUIRED: in preemptive code, add code to handle synchronization primitives
void svCallIsr()
{
 uint8_t svc=getsvcnumber();
 uint32_t i= getR0();
 switch(svc)
 {
   case 40 : // YEILD
     NVIC_INT_CTRL_R=NVIC_INT_CTRL_PEND_SV;  // pend sv bit set here
     break;
   case 41: // sleep
       tcb[taskCurrent].ticks=i;
       tcb[taskCurrent].state=STATE_DELAYED;
       NVIC_INT_CTRL_R=NVIC_INT_CTRL_PEND_SV;
       break;
   case 42:  //wait  //step 9

          if(semaphores[i].count >0)
          {
              semaphores[i].count--;

          }
          else
          {
              semaphores[i].processQueue[semaphores[i].queueSize]=taskCurrent;
              semaphores[i].queueSize++;
              tcb[taskCurrent].state=STATE_BLOCKED;
              tcb[taskCurrent].s =i;
              NVIC_INT_CTRL_R=NVIC_INT_CTRL_PEND_SV;
          }
       break;
   case 43: //post
       semaphores[i].count++;
       if(semaphores[i].queueSize>0)
       {
            //tcb[taskCurrent].state= STATE_READY;
           tcb[semaphores[i].processQueue[0]].state=STATE_READY;
           semaphores[i].processQueue[0]=semaphores[i].processQueue[1];
           semaphores[i].processQueue[1]=NULL;
           semaphores[i].queueSize--;
           semaphores[i].count--;    // no need to add pendsv
       }
     break;   // *****************

 }


}

//-----------------------------------------------------------------------------
// Subroutines
//-----------------------------------------------------------------------------

// Initialize Hardware
// REQUIRED: Add initialization for blue, orange, red, green, and yellow LEDs
//           6 pushbuttons
void initHw()
{

      // Configure HW to work with 16 MHz XTAL, PLL enabled, system clock of 40 MHz
      SYSCTL_RCC_R = SYSCTL_RCC_XTAL_16MHZ | SYSCTL_RCC_OSCSRC_MAIN | SYSCTL_RCC_USESYSDIV | (4 << SYSCTL_RCC_SYSDIV_S);

         // Enable clocks
          SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R5;
          SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R4; // Port F,E,A,C,D
          SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R3;
          SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R2;
          SYSCTL_RCGCGPIO_R |= SYSCTL_RCGCGPIO_R0;
        _delay_cycles(3);

        // Port D unlock
        GPIO_PORTD_LOCK_R =0x4C4F434B;
        GPIO_PORTD_CR_R |= PB5_MASK;
        GPIO_PORTD_DIR_R &= ~(PB4_MASK|PB5_MASK);
        GPIO_PORTD_PUR_R |= PB4_MASK|PB5_MASK;
        GPIO_PORTD_DEN_R |=PB4_MASK|PB5_MASK;
        GPIO_PORTA_DIR_R |= RED_LED_MASK |ORANGE_LED_MASK|YELLOW_LED_MASK;
        GPIO_PORTF_DIR_R |= BLUE_LED_MASK;
        GPIO_PORTE_DIR_R |= GREEN_LED_MASK;
        GPIO_PORTC_DIR_R &= ~(PB0_MASK|PB1_MASK|PB2_MASK|PB3_MASK);
        GPIO_PORTA_DR2R_R |= RED_LED_MASK |ORANGE_LED_MASK|YELLOW_LED_MASK;
        GPIO_PORTF_DR2R_R |= BLUE_LED_MASK;
        GPIO_PORTE_DR2R_R |= GREEN_LED_MASK;
        GPIO_PORTA_DEN_R |=RED_LED_MASK |ORANGE_LED_MASK|YELLOW_LED_MASK;
        GPIO_PORTC_DEN_R |=PB0_MASK|PB1_MASK|PB2_MASK|PB3_MASK;
        GPIO_PORTE_DEN_R |=GREEN_LED_MASK;
        GPIO_PORTF_DEN_R |=BLUE_LED_MASK;
        GPIO_PORTC_PUR_R |= PB0_MASK|PB1_MASK|PB2_MASK|PB3_MASK;
        //systick on
        NVIC_ST_RELOAD_R=39999;    // for 1khz @40mhz
        NVIC_ST_CURRENT_R=0 ;      //  write down any value to clear
        NVIC_ST_CTRL_R |=NVIC_ST_CTRL_CLK_SRC|NVIC_ST_CTRL_INTEN |NVIC_ST_CTRL_ENABLE;





}

// REQUIRED: add code to return a value from 0-63 indicating which of 6 PBs are pressed
uint8_t readPbs()
{int a =0;
    if(PB0==0)
     {
        a=1;
     }
    if(PB1==0)
     {
        a=a+2;
     }
    if(PB2==0)
        {
            a=a+4;
        }
    if(PB3==0)
        {
            a=a+8;
        }
    if(PB4==0)
        {
            a=a+16;
        }
    if(PB5==0)
    {
        a=a+32;
    }
#ifdef DEBUG
    sprintf(str, "a value: %i\n", a);
    putsUart0(str);
    putsUart0("\n");
#endif
    return a;
//
}

//-----------------------------------------------------------------------------
// YOUR UNIQUE CODE
// REQUIRED: add any custom code in this space
//-----------------------------------------------------------------------------

// ------------------------------------------------------------------------------
//  Task functions
// ------------------------------------------------------------------------------

// one task must be ready at all times or the scheduler will fail
// the idle task is implemented for this purpose
//----------------------------------------------------------------------------------------------------------------------------

void idle()
{

    while(true)
    {

        ORANGE_LED = 1;
        waitMicrosecond(1000);
        ORANGE_LED = 0;
//#ifdef DEBUG
//        __asm("     MOV R0,#100");
//        __asm("     MOV R1,#101");
//        __asm("     MOV R2,#102");
//        __asm("     MOV R3,#103");
//        __asm("     MOV R4,#104");
//        __asm("     MOV R5,#105");
//        __asm("     MOV R6,#106");
//        __asm("     MOV R7,#107");
//        __asm("     MOV R8,#108");
//        __asm("     MOV R9,#109");
//        __asm("     MOV R10,#110");
//        __asm("     MOV R11,#111");
//        __asm("     MOV R12,#112");
//#endif


        yield();
    }
}

void flash4Hz()
{
    while(true)
    {
        GREEN_LED ^= 1;
        sleep(125);
    }
}

void oneshot()
{
    while(true)
    {
        wait(flashReq);
        YELLOW_LED = 1;
        sleep(1000);
        YELLOW_LED = 0;
    }
}

void partOfLengthyFn()
{
    // represent some lengthy operation
    waitMicrosecond(990);
    // give another process a chance to run
    yield();
}

void lengthyFn()
{
    uint16_t i;
    while(true)
    {
        wait(resource);
        for (i = 0; i < 5000; i++)
        {
            partOfLengthyFn();
        }
        RED_LED ^= 1;
        post(resource);
    }
}

void readKeys()
{
    uint8_t buttons;
    while(true)
    {
        wait(keyReleased);
        buttons = 0;
        while (buttons == 0)
        {
            buttons = readPbs();
            yield();
        }
        post(keyPressed);
        if ((buttons & 1) != 0)
        {
            YELLOW_LED ^= 1;
            RED_LED = 1;
        }
        if ((buttons & 2) != 0)
        {
            post(flashReq);
            RED_LED = 0;
        }
        if ((buttons & 4) != 0)
        {
            restartThread(flash4Hz);
        }
        if ((buttons & 8) != 0)
        {
            destroyThread(flash4Hz);
        }
        if ((buttons & 16) != 0)
        {
            setThreadPriority(lengthyFn, 4);
        }
        yield();
    }
}

void debounce()
{
    uint8_t count;
    while(true)
    {
        wait(keyPressed);
        count = 10;
        while (count != 0)
        {
            sleep(10);
            if (readPbs() == 0)
                count--;
            else
                count = 10;
        }
        post(keyReleased);
    }
}

void uncooperative()
{
    while(true)
    {
        while (readPbs() == 32)
        {
        }
        yield();
    }
}

void important()
{
    while(true)
    {
        wait(resource);
        BLUE_LED = 1;
        sleep(1000);
        BLUE_LED = 0;
        post(resource);
    }
}

void freezdata()
{
    uint8_t i;
    uint64_t a;
    uint32_t totalcpu =CPUtotal;  // cpu time store for all data calculation
    for(i=0;i<MAX_TASKS;i++)
    {
        Freezdata[i]=  Task_timer[i];
    }
    for(i=0;i<MAX_TASKS;i++)
    {
        a=Freezdata[i]*100000;
        a=a/totalcpu;
        Freezdata[i]=a;

    }

}

uint8_t stringcmp( const char* cmd, char *check)   // source internet for understanding but not copied
{

    while(*cmd==*check)
    {
       if(*cmd=='\0'&&*check=='\0')
       { return 0;}

           cmd++;
           check++;


    }
//    if(*cmd=='\0'&&*check=='\0')
//     {return 0;}
    return 2;
}
// REQUIRED: add processing for the shell commands through the UART here
void shell()
{
    while (true)
    {

             bool valid =false;

            USER_DATA data;
            getsUart0(&data);

           // display data here

         //putsUart0(data.buffer);
         parseFields(&data);
         putsUart0("\n\r");


 //-------
 // 6 commands run here
          if (isCommand(&data, "reboot", 0))
              {
               putsUart0("\n\r");
               NVIC_APINT_R=NVIC_APINT_VECTKEY |NVIC_APINT_SYSRESETREQ;   // enable bit of NVIC M4F  reg
               valid= true;
              }
          if(isCommand(&data,"ps",0))
          {  putsUart0("\rPID \t   CPU%\t    State\t      Name \t      Semaphore blocked\r\n");
             putsUart0("\r---------------------------------------------------------------\r\n");


              uint8_t i;
              for(i=0;i<MAX_TASKS;i++)
              {
                  if(tcb[i].state!=STATE_INVALID )
                  { sprintf(str,"%d\t",tcb[i].pid);
                    putsUart0(str);
                    sprintf(str,"%2d.%3d  %%\t",Freezdata[i]/1000,Freezdata[i]%1000);
                    putsUart0(str);
                    if(tcb[i].state==1)
                       putsUart0("Unrun\t");
                    if(tcb[i].state==2)
                        putsUart0("Ready\t");
                    if(tcb[i].state==3)
                        putsUart0("Delay\t");
                    if(tcb[i].state==4)
                        putsUart0("blocked\t");
                    if(tcb[i].state==5)
                        putsUart0("Hold\t");

                    putsUart0("    ");
                    putsUart0(tcb[i].name);
                    putsUart0("    ");
                    putsUart0("\t");
                    if(tcb[i].s==1)
                        putsUart0("kyePress\t");
                    if(tcb[i].s==2)
                        putsUart0("kyeReleas\t");
                    if(tcb[i].s==3)
                        putsUart0("flash4Hz\t");
                    if(tcb[i].s==4)
                           putsUart0("resource\t");
                    if(tcb[i].s==0)
                        putsUart0("\t");


                    putsUart0("\r\n");





                  }
              }

               valid= true;
          }

          if(isCommand(&data,"sched",1))
          {
              char* sr = getFieldString(&data, 1);
               if(stringcmp("rr",sr)==0)
               {
                   valid=true;
                   RoundRobin=true;
                   putsUart0("\rRoundRobin On\r\n");
               }
               if(stringcmp("prio",sr)==0)
               {
                   valid=true;
                   RoundRobin=false;
                   putsUart0("\rPriority On\r\n");
               }
          }
          if(isCommand(&data,"preem",1))
          {
              char* sr = getFieldString(&data, 1);
              if(stringcmp("on",sr)==0)
             {
                  valid=true;
                  Preemption=true;

                  putsUart0("\rPreemptio On\r\n");
              }
              if(stringcmp("off",sr)==0)
              {
                valid=true;
                Preemption=false;
                putsUart0("\rPreemption Off\r\n");
              }
          }
          if(isCommand(&data,"pidof",1))
          { uint8_t i;

           char* sr = getFieldString(&data, 1);
           for(i=0;i<MAX_TASKS;i++)
           {
             if(stringcmp(tcb[i].name,sr)==0)
             {
               valid=true;
               sprintf(str, "pid number: %d\r\n",tcb[i].pid);
               putsUart0(str);
               break;
             }
           }
          }
          if(isCommand(&data,"kill",1))  //kill by pid number
          {   uint8_t i;
              uint32_t pid = getFieldInteger(&data, 1);
              for(i=0;i<MAX_TASKS;i++)
              {
                  if(tcb[i].pid==pid)
                  {   valid=true;
                      destroyThread(tcb[i].pFn);
                      break;
                  }
              }

              if(!valid)
              {
                valid=true;
                putsUart0("\rwrong pid number\r\n");
              }
          }
          if(isCommand(&data,"run",1))
          { uint8_t i;
          bool run =false;
              char* sr = getFieldString(&data, 1);
              for(i=0;i<MAX_TASKS;i++)
              {
                  if((stringcmp(tcb[i].name,sr)==0) && sr!='\0')
                  {       valid=true;
                      if(tcb[i].state==STATE_HOLD)
                      {
                          restartThread(tcb[i].pFn);
                          //putsUart0("\rNow Task can run\r\n");
                          run=true;
                          break;
                      }
                  }

              }
              if(run==false &&valid ==true)
              {
                  putsUart0("\rTask is already running\r\n");
              }
          }
          if(isCommand(&data,"ipcs",0))
          {  uint8_t i,p;
              putsUart0("\rIndex     NAME        Count     Que_size    Task in Que \r\n");
              putsUart0("\r----    --------    ------      -----       ----------\r\n");
              for(i=1;i<MAX_SEMAPHORES;i++)
              {
                  sprintf(str,"%d\t %s\t %d\t %d\t ",i,semaphores[i].S_name,semaphores[i].count,semaphores[i].queueSize);
                   putsUart0(str);
                   valid=true;
                   if(semaphores[i].queueSize==0)
                   {
                       putsUart0("None\r\n");
                   }
                   if(semaphores[i].queueSize>0)
                   {
                       for(p=0;p<MAX_TASKS;p++)
                       {
                           if(p==semaphores[i].processQueue[0])
                           {
                               putsUart0(tcb[p].name);
                                       putsUart0("\r\n");
                           }
                       }
                   }
              }
          }

          if (!valid)
             {
                putsUart0("Invalid command\r\n");
             }
          putsUart0("\r>");

          yield();


    }
}

//-----------------------------------------------------------------------------
// Main
//-----------------------------------------------------------------------------

int main(void)
{
    bool ok;

    // Initialize hardware
    initHw();
    initUart0();
    initRtos();

    // Setup UART0 baud rate
    setUart0BaudRate(115200, 40e6);

    putsUart0("\n\r RTOS Project_NEEL Shah \r\n");
    putsUart0("\r>");

    // Power-up flash
    GREEN_LED = 1;

    waitMicrosecond(250000);
    GREEN_LED = 0;

    waitMicrosecond(250000);

    // Initialize semaphores
    createSemaphore(keyPressed,"KeyPressed" ,1);
    createSemaphore(keyReleased,"KeyReleased" ,0);
    createSemaphore(flashReq,"FlashReq", 5);
    createSemaphore(resource,"Resource", 1);

    // Add required idle process at lowest priority
       ok =  createThread(idle, "Idle", 7, 1024);


//    // Add other processes
      ok &= createThread(lengthyFn, "LengthyFn", 6, 1024);
       ok &= createThread(flash4Hz, "Flash4Hz", 4, 1024);
       ok &= createThread(oneshot, "OneShot", 2, 1024);
       ok &= createThread(readKeys, "ReadKeys", 6, 1024);
       ok &= createThread(debounce, "Debounce", 6, 1024);
       ok &= createThread(important, "Important", 0, 1024);
       ok &= createThread(uncooperative, "Uncoop", 6, 1024);
        ok &= createThread(shell, "Shell", 6, 4096);

    // Start up RTOS
    if (ok)
        startRtos(); // never returns
    else
        RED_LED = 1;

    return 0;
}


