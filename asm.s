
 .def setPSP
 .def setASP
 .def swPush
 .def getPSP
 .def swPop
 .def dummyPush
 .def getsvcnumber
 .def getR0



.thumb
.const




.text

setPSP:

 MSR PSP, R0
 BX LR


setASP:
  MOV R1,#2
  MSR CONTROL,R1
  ISB
  BX LR
getPSP:
   MRS R0,PSP
   BX LR
swPush:
    MRS R0,PSP
    SUB R0,R0,#4
    STR R4,[R0]
    SUB R0,R0,#4
    STR R5,[R0]
    SUB R0,R0,#4
    STR R6,[R0]
    SUB R0,R0,#4
    STR R7,[R0]
    SUB R0,R0,#4
    STR R8,[R0]
    SUB R0,R0,#4
    STR R9,[R0]
    SUB R0,R0,#4
    STR R10,[R0]
    SUB R0,R0,#4
    STR R11,[R0]
    MSR PSP,R0
    ISB
    BX LR




swPop:
    MRS R0,PSP
    LDR R11,[R0]
    ADD R0,R0,#4
    LDR R10,[R0]
    ADD R0,R0,#4
    LDR R9,[R0]
    ADD R0,R0,#4
    LDR R8,[R0]
    ADD R0,R0,#4
    LDR R7,[R0]
    ADD R0,R0,#4
    LDR R6,[R0]
    ADD R0,R0,#4
    LDR R5,[R0]
    ADD R0,R0,#4
    LDR R4,[R0]
    ADD R0,R0,#4
    MSR PSP,R0
    ISB
    BX LR




dummyPush:
     MOV R1,R0              ; STORE A pFN VALUE FOR PC
     MOV R2,#0x01000000     ; FOR XPSR VALUE
     MOV R12,#0
     MRS R3,PSP             ; R3 store psp
     SUB R3,R3,#4
     STR R2,[R3]            ;  XPSR VALUE PUSH ON PSP
     SUB R3,R3,#4
     STR R1,[R3]    		; PC VALUE STORE WITHOUT OR OPERATION BECAUSE ITS ALREADY ODD
     SUB R3,R3,#4
     STR R12,[R3]   		; 0 WILL STORE IN LR Address ON PSP
     SUB R3,R3,#4
     MOV R12,#1
     STR R12,[R3]   		; 1 WILL STORE IN R12 Address ON PSP push
     SUB R3,R3,#4
     ADD R12,R12,#1
     STR R12,[R3]           ; 2 WILL BE STORE IN R3 Address on psp push
     SUB R3,R3,#4
     ADD R12,R12,#1
     STR R12,[R3]			;3 WILL ......R2....PSP
     SUB R3,R3,#4
     ADD R12,R12,#1
     STR R12,[R3]			;4.......R1...PSP
     SUB R3,R3,#4
     MOV R12,#5
     STR R12,[R3]			;5... R0...PSP.. PUSH
     MSR PSP,R3             ;
     BX LR



getR0:
    MRS R1,PSP
    ISB
    LDR R0,[R1]
    BX LR

getsvcnumber:
  MRS R1,PSP
  ISB
  LDR R1,[R1,#24]
  LDRB R1,[R1,#-2]   ;for this line referance https://www.iotality.com/armcm-svc/
  AND R1,#0x00FF   ; for get last 8bit  REQUIRED
  MOV R0,R1
  BX LR


.endm











